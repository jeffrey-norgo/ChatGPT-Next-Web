export const COMMON_PROVIDER_CONFIG = {
  customModels: "",
  models: [] as string[],
  autoFetchModels: false, // fetch available models from server or not
};
